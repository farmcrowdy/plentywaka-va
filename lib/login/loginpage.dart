import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_indicator_button/progress_button.dart';
import '../dashboard/dashboardpage.dart';
import '../api/apis.dart';
import '../models/loginpojo.dart';


final _usernameController = new TextEditingController();
final _busController = new TextEditingController();
final _companyCodeController = new TextEditingController();

bool _obscureText = true;

String _password;

class LoginPage extends StatelessWidget {



  // Toggles the password show status
  void _toggle() {
 //   setState(() {
   //   _obscureText = !_obscureText;
    //});
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(body: Builder(
            builder: (context) => Center(child: SingleChildScrollView(

                padding: EdgeInsets.all(32),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          SizedBox(
                            height: 16,
                          ),
                          Image.asset(
                            "assets/pw_logo_jpg.jpg",
                            width: 100,
                            height: 100,
                          ),
                          SizedBox(height: 16.0),
                          Text(
                            "Welcome to PlentyWaka Corporate,",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 26),
                          ),
                          SizedBox(height: 16.0),
                          Text(
                            'Sign in to continue',
                            style: TextStyle(fontSize: 18, color: Colors.grey),
                          ),
                          SizedBox(height: 16.0),

                          TextField(
                              style: TextStyle(fontSize: 18, color: Colors.black),
                              controller: _busController,
                              keyboardType: TextInputType.number,
                              maxLength: 3,
                              textInputAction: TextInputAction.next,
                              maxLengthEnforced: true,
                              decoration: InputDecoration(
                                  icon: new Icon(Icons.directions_bus, color: Colors.grey,),
                                  labelText: 'Your Bus ID',
                                  labelStyle: TextStyle(color: Colors.grey, fontSize: 14)),
                              obscureText: false),



                          SizedBox(height: 16.0),

                          TextField(
                              style: TextStyle(fontSize: 18, color: Colors.black),
                              controller: _companyCodeController,
                              keyboardType: TextInputType.number,
                              maxLength: 4,
                              textInputAction: TextInputAction.next,
                              maxLengthEnforced: true,
                              decoration: InputDecoration(
                                  icon: new Icon(Icons.work, color: Colors.grey,),
                                  labelText: 'Pick-up Company Code',
                                  labelStyle: TextStyle(color: Colors.grey, fontSize: 14)),
                              obscureText: false),

                          SizedBox(height: 26.0),
                          new SizedBox(
                            width: double.infinity,
                            height: 48,
                            child: ProgressButton(
                              progressIndicatorSize: 26,
                              progressIndicatorColor: Colors.white,
                              color: Colors.yellow[700],
                              borderRadius: BorderRadius.circular(20),
                              child: Text(
                                "Login",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                ),
                              ),
                              onPressed: (AnimationController controller) {
                               // _asyncLoader(context, 1, controller, observer);
                                doLogin(controller, context);
                              },
                            ),
                          ),
                        ])))));
  }


}
void doLogin(AnimationController controller, BuildContext context) async {
  controller.forward();

  final myResponseMap = await doLoginForBus(_busController.text, _companyCodeController.text);
  var vaLoginModel = myResponseMap.keys.first;
  var companyInfo = myResponseMap.values.first;
  if(vaLoginModel == null) {
    controller.reverse();
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(
          'Something went wrong. Please check your internet connection and try again.'),
      duration: Duration(seconds: 2),
    ));
  }

  else {
    if (vaLoginModel.data == null) {
      controller.reverse();
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
            'Wrong company code or bus number. Please check again'),
        duration: Duration(seconds: 2),
      ));
    }

    else {
      controller.reverse();
      Navigator.push(context, MaterialPageRoute(builder: (context) =>
          DashboardPage(vaLoginModel, _busController.text, companyInfo,)));
    }
  }

}
