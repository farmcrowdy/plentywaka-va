// To parse this JSON data, do
//
//     final vaLoginModel = vaLoginModelFromJson(jsonString);

import 'dart:convert';

VaLoginModel vaLoginModelFromJson(String str) => VaLoginModel.fromJson(json.decode(str));

String vaLoginModelToJson(VaLoginModel data) => json.encode(data.toJson());

class VaLoginModel {
  Data data;
  String message;

  VaLoginModel({
    this.data,
    this.message,
  });

  factory VaLoginModel.fromJson(Map<String, dynamic> json) => VaLoginModel(
    data: Data.fromJson(json["data"]),
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "data": data.toJson(),
    "message": message,
  };
}

class Data {
  int id;
  DateTime createdAt;
  DateTime updatedAt;
  int userId;
  int inventoryId;
  String startLocation;
  String endLocation;
  DateTime tripDate;

  Data({
    this.id,
    this.createdAt,
    this.updatedAt,
    this.userId,
    this.inventoryId,
    this.startLocation,
    this.endLocation,
    this.tripDate,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    userId: json["user_id"],
    inventoryId: json["inventory_id"],
    startLocation: json["start_location"],
    endLocation: json["end_location"],
    tripDate: DateTime.parse(json["trip_date"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "user_id": userId,
    "inventory_id": inventoryId,
    "start_location": startLocation,
    "end_location": endLocation,
    "trip_date": tripDate.toIso8601String(),
  };
}
