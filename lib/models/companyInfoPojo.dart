// To parse this JSON data, do
//
//     final companyInfo = companyInfoFromJson(jsonString);

import 'dart:convert';

CompanyInfo companyInfoFromJson(String str) => CompanyInfo.fromJson(json.decode(str));

String companyInfoToJson(CompanyInfo data) => json.encode(data.toJson());

class CompanyInfo {
  Data data;

  CompanyInfo({
    this.data,
  });

  factory CompanyInfo.fromJson(Map<String, dynamic> json) => CompanyInfo(
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "data": data.toJson(),
  };
}

class Data {
  int id;
  String name;
  String email;
  dynamic emailVerifiedAt;
  String createdAt;
  String updatedAt;
  dynamic address;
  dynamic phoneNumber;
  String companyCode;
  int roleId;

  Data({
    this.id,
    this.name,
    this.email,
    this.emailVerifiedAt,
    this.createdAt,
    this.updatedAt,
    this.address,
    this.phoneNumber,
    this.companyCode,
    this.roleId,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"],
    name: json["name"],
    email: json["email"],
    emailVerifiedAt: json["email_verified_at"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    address: json["address"],
    phoneNumber: json["phone_number"],
    companyCode: json["company_code"],
    roleId: json["role_id"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "email_verified_at": emailVerifiedAt,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "address": address,
    "phone_number": phoneNumber,
    "company_code": companyCode,
    "role_id": roleId,
  };
}
