// To parse this JSON data, do
//
//     final vsControlPojo = vsControlPojoFromJson(jsonString);

import 'dart:convert';

List<VsControlPojo> vsControlPojoFromJson(String str) => new List<VsControlPojo>.from(json.decode(str).map((x) => VsControlPojo.fromJson(x)));

String vsControlPojoToJson(List<VsControlPojo> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class VsControlPojo {
  int version;
  String topic;
  String content;
  String linkAndroid;
  String linkIos;

  VsControlPojo({
    this.version,
    this.topic,
    this.content,
    this.linkAndroid,
    this.linkIos,
  });

  factory VsControlPojo.fromJson(Map<String, dynamic> json) => new VsControlPojo(
    version: json["version"],
    topic: json["topic"],
    content: json["content"],
    linkAndroid: json["link_android"],
    linkIos: json["link_ios"],
  );

  Map<String, dynamic> toJson() => {
    "version": version,
    "topic": topic,
    "content": content,
    "link_android": linkAndroid,
    "link_ios": linkIos,
  };
}
