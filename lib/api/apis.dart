import 'dart:collection';

import '../models/loginpojo.dart';
import 'package:http/http.dart' as http;
import '../models/companyInfoPojo.dart';
const baseUrl = "https://corporate.plentywaka.com";
const authKey = "7f0WlIM0kU8aVilW9GbKRqxmsFJjP3lG0ghPr5dStGmOY8Z7KlMLEuATJrGwfgJA";





Future<Map<VaLoginModel, CompanyInfo>> doLoginForBus(String busNumber, String companyCode) async {

  var initialValModel, companyData;
 await http.post(
    baseUrl + '/api/va-login',
    body: { "company-code": companyCode, "bus-number": busNumber },
    headers: {"x-authorization": authKey },
  ).then((responseBody) async {
    var newModel = vaLoginModelFromJson(responseBody.body);
    initialValModel = newModel;


   if(responseBody.statusCode == 200) {

     print("user id is: "+newModel.data.userId.toString());
     await http.get(baseUrl + "/api/company/" + newModel.data.userId.toString(),
       headers: {"x-authorization": authKey,}, ).then((onValue) {
        // print("answer is: "+onValue.body);
       companyData = companyInfoFromJson(onValue.body);

     });
   }

 });


  Map<VaLoginModel, CompanyInfo> myMap = new HashMap();
  myMap.putIfAbsent(initialValModel, ()=>companyData);
  return myMap;
}


Future<int> doCheckIn(String busNumber, String phoneNumber, int tripId) async {
  //int phoneNumber2 = int.parse(phoneNumber);
  //int busNumber2 = int.parse(busNumber);

  int status = 500;
 await http.post(
    baseUrl + '/api/checkin-staff',
    body: { "phone-number": phoneNumber, "bus-number": busNumber, "trip-id": tripId.toString() },
    headers: {"x-authorization": authKey },
  ).then((responseBody) {
   // print("login info has "+ responseBody.statusCode.toString() + " with message: "+responseBody.body);
    status = responseBody.statusCode;
  }).catchError((error){
    print(error);
    status = 504;
 });

  return status;
}


Future<int> doCheckOut(String busNumber, String phoneNumber, int tripId) async {
 // int busNumber2 = int.parse(busNumber);

  int status = 500;
  await http.post(
    baseUrl + '/api/checkout-staff',
    body: { "phone-number": phoneNumber, "bus-number": busNumber, "trip-id": tripId.toString() },
    headers: {"x-authorization": authKey },
  ).then((responseBody) {
    print("login info has "+ responseBody.statusCode.toString() + " with message: "+responseBody.body);
    status = responseBody.statusCode;
  });

  return status;
}