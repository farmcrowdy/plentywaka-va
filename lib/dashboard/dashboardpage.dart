import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pw_corp_va/api/apis.dart';
import 'package:pw_corp_va/colors.dart';
import 'package:majascan/majascan.dart';
import '../manualcheck/manualcheckout.dart';
import '../models/loginpojo.dart';
import '../models/companyInfoPojo.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:url_launcher/url_launcher.dart';

class DashboardPage extends StatefulWidget {
  final VaLoginModel vaLoginModel;
  final String busNumber;
  final CompanyInfo companyInfo;
  DashboardPage(this.vaLoginModel, this.busNumber, this.companyInfo);

  @override
  DashboardPageState createState ()=> new DashboardPageState(vaLoginModel, busNumber, companyInfo);

}


class DashboardPageState extends State<DashboardPage> {
  final VaLoginModel vaLoginModel;
  final String busNumber;
  final CompanyInfo companyInfo;
  DashboardPageState(this.vaLoginModel, this.busNumber, this.companyInfo);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
    appBar: AppBar(
      title: null,
      elevation: 0,
      backgroundColor: primaryColor,
      automaticallyImplyLeading: false,
    ),
      backgroundColor: primaryColor,
      body: Builder(builder: (context)=> SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            
            Padding(
              padding: EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
              Text("Welcome,", style: TextStyle(fontSize: 18),),
              Text("Bus $busNumber", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 30),),

                  SizedBox(height: 36,),
                  Text("Company name"),
                  Text(companyInfo.data.name,  textAlign: TextAlign.start, style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold, color: Colors.black),),
                  SizedBox(height: 16,),

              GestureDetector(
                  onTap: ()=>MapsLauncher.launchQuery(vaLoginModel.data.startLocation),
                  child:Text("Start location (Tap to see map)")),
              GestureDetector(
                onTap: ()=>MapsLauncher.launchQuery(vaLoginModel.data.startLocation),
                child:Text(vaLoginModel.data.startLocation,  textAlign: TextAlign.start, style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold, color: Colors.black),),),
              SizedBox(height: 16,),



              GestureDetector(
                  onTap: ()=>MapsLauncher.launchQuery(vaLoginModel.data.endLocation),
                  child:Text("End Location (Tap to see map)")),
              GestureDetector(
                onTap: ()=>MapsLauncher.launchQuery(vaLoginModel.data.endLocation),
                child:Text(vaLoginModel.data.endLocation,  textAlign: TextAlign.start, style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold, color: Colors.black),),),
              SizedBox(height: 16,),

              GestureDetector(
                  onTap: ()=>launch("tel://"+companyInfo.data.phoneNumber),
                  child: Text("Contact Number (Tap to call contact)")),
              GestureDetector(
                onTap: ()=>launch("tel://"+companyInfo.data.phoneNumber),
                child: Text((companyInfo.data.phoneNumber != null) ? companyInfo.data.phoneNumber : "Not Available",  textAlign: TextAlign.start, style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold, color: Colors.black),),),
              SizedBox(height: 16,),


            ],),),
            

            Container(
              height: 700,
              padding: EdgeInsets.all(24),
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.only(topLeft: Radius.circular(32), topRight: Radius.circular(32))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text("Check In", style: TextStyle(color: Colors.yellow[800], fontSize: 20),),
                  SizedBox(height: 8,),
                  Text("Click the button below to check in rider with QR, or long press to check in manually",
                    style: TextStyle(color: Colors.black),),

                  SizedBox(height: 16,),

                  SizedBox(width: double.infinity,
                    height: 48,
                    child:
                    GestureDetector(
                      onLongPress: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>ManualCheckoutPage("Check In Rider", vaLoginModel, busNumber))),
                      child:

                    RaisedButton.icon(
                        onPressed: () async {
                          String qrResult = await MajaScan.startScan(
                              title: "Scanning To Check In",
                              barColor: Colors.white,
                              titleColor: Colors.black,
                              qRCornerColor: Colors.yellow[700],
                              qRScannerColor: Colors.yellow[700],
                              flashlightEnable: true
                          );

                      callBackFromQRCode(1, qrResult, context);

                        },
                        icon: SizedBox(width: 0, height: 0,),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
                        color: Colors.black,
                        label: Text("Check in rider", style: TextStyle(color: Colors.white),)),)),



                  SizedBox(height: 42,),
                  Text("Check out", style: TextStyle(color: Colors.yellow[800], fontSize: 20),),
                  SizedBox(height: 8,),
                  Text("Click the button below to check out rider with QR, or long press to check out manually",
                    style: TextStyle(color: Colors.black),),

                  SizedBox(height: 16,),

                  SizedBox(width: double.infinity,
                    height: 48,
                    child:
                    GestureDetector(
                      onLongPress: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>ManualCheckoutPage("Check Out Rider", vaLoginModel, busNumber))),
                      child: RaisedButton.icon(
                        onPressed: () async {
                          String qrResult = await MajaScan.startScan(
                              title: "Scanning To Check out",
                              barColor: Colors.white,
                              titleColor: Colors.black,
                              qRCornerColor: Colors.yellow[700],
                              qRScannerColor: Colors.yellow[700],
                              flashlightEnable: true
                          );

                          callBackFromQRCode(2, qrResult, context);
                        },
                        icon: SizedBox(width: 0, height: 0,),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
                        color: Colors.black,
                        label: Text("Check out rider", style: TextStyle(color: Colors.white),)),))

                ],
              ),
            ),
          ],
        ),
      )),
    );
  }


  void callBackFromQRCode(int type, String phoneNumber, BuildContext context) async {
    ProgressDialog dialog = new ProgressDialog(context, type: ProgressDialogType.Normal);
    switch(type) {
      case 1:
        dialog.style(message: "Checking in, please wait...");
        if(!dialog.isShowing()) {
          dialog.show();

          final actionStatus = await doCheckIn(busNumber, phoneNumber, vaLoginModel.data.id);
          dialog.dismiss();
          if(actionStatus == 200) {
            doResponseFeedback(context: context, title: "Check in successful", content: "Staff has been checked-in successfully", buttonString:"OK");
          }
          else {
            doResponseFeedback(context: context, title: "Try Again", content: "Something went wrong, while trying to check in staff", buttonString:"Try Again");
          }


        }

        break;
      case 2:
        dialog.style(message: "Checking out, please wait...");
        if(!dialog.isShowing()) {
          dialog.show();
          final actionStatus = await doCheckOut(busNumber, phoneNumber, vaLoginModel.data.id);
          dialog.dismiss();
          if(actionStatus == 200) {
            doResponseFeedback(context: context, title: "Check out successful", content: "Staff has been checked-out successfully", buttonString:"OK");
          }
          else {
            doResponseFeedback(context: context, title: "Try Again", content: "Something went wrong, while trying to check out staff", buttonString:"Try Again");
          }
        }
        break;
    }

  }

  doResponseFeedback({BuildContext context, String title, content, buttonString }) async {
    await showDialog(
        context:  context,
        barrierDismissible: true,
        builder: (context) => AlertDialog(
      title:  Text(title, style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),),
      content: Text(content),
      actions: [
        new FlatButton(
          child: new Text(buttonString, style: TextStyle(color: Colors.yellow[800], fontWeight: FontWeight.bold),),
          onPressed: () {
            Navigator.pop(context);
           // Navigator.push(context, MaterialPageRoute(builder: (context) => PayOptionsPage()));
          }
          ,
        ),
      ],
    ),
    );
  }


}

