import 'package:flutter/material.dart';
import '../colors.dart';
import '../models/loginpojo.dart';
import '../api/apis.dart';
import 'package:progress_indicator_button/progress_button.dart';


var _staffPhoneController = new TextEditingController();
class ManualCheckoutPage extends StatelessWidget {
  final String checkType;
  final VaLoginModel vaLoginModel;
  final String busNumber;

  ManualCheckoutPage(this.checkType, this.vaLoginModel, this.busNumber);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(leading: IconButton(
          icon: Icon(Icons.keyboard_backspace, color: Colors.black,),
          onPressed: () {
            _staffPhoneController.clear();
            Navigator.pop(context);}),
          backgroundColor: primaryColor,
          elevation: 0,
        ),

        backgroundColor: primaryColor,
        body: Builder(builder: (context)=>Center(child: SafeArea(
          minimum: EdgeInsets.all(26),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.check_box, color: Colors.black, size: 200,),
              SizedBox(height: 16,),
              Text("Enter Rider's Staff Pin", style: TextStyle(
                  color: Colors.black,
                  fontSize: 22,
                  fontWeight: FontWeight.bold),),
              SizedBox(height: 8,),
              Text("Enter the rider's staff pin in the field below to check " +
                  checkType.toLowerCase(), textAlign: TextAlign.center,),

              SizedBox(height: 16,),

              Container(
                width: double.infinity,
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(color: Colors.white,
                    borderRadius: BorderRadius.circular(6)),
                child: TextField(
                    style: TextStyle(fontSize: 18, color: Colors.black),
                    controller: _staffPhoneController,
                    keyboardType: TextInputType.phone,
                    maxLength: 11,
                    textInputAction: TextInputAction.next,
                    maxLengthEnforced: true,
                    decoration: InputDecoration(
                        icon: new Icon(Icons.person_pin, color: Colors.grey,),
                        labelText: 'Staff Phone number',
                        labelStyle: TextStyle(
                            color: Colors.grey, fontSize: 14)),
                    obscureText: false),
              ),

              SizedBox(height: 32,),
          SizedBox(
            width: double.infinity, height: 48, child:
              ProgressButton(
                progressIndicatorSize: 26,
                progressIndicatorColor: Colors.white,
                color: Colors.black,
                borderRadius: BorderRadius.circular(20),
                child:  Text(checkType, style: TextStyle(color: Colors.white),),
                onPressed: (AnimationController controller) {
                  // _asyncLoader(context, 1, controller, observer);
                  (checkType == "Check In Rider")
                      ?
                  callBackFromQRCode(controller, 1, _staffPhoneController.text, context)
                      :
                  callBackFromQRCode(controller, 2, _staffPhoneController.text, context);

                },
              ),),

            ],
          ),
        ),))
    );
  }


  void callBackFromQRCode(AnimationController controller, int type, String phoneNumber, BuildContext context) async {
    switch (type) {
      case 1:
        controller.forward();

        final actionStatus = await doCheckIn(busNumber, phoneNumber, vaLoginModel.data.id);
         controller.reverse();
          if (actionStatus == 200) {
            doResponseFeedback(
                context: context,
                title: "Check in successful",
                content: "Staff has been checked-in successfully",
                buttonString: "OK");
          }
          else {
            doResponseFeedback(
                context: context,
                title: "Try Again",
                content: "Something went wrong, while trying to check in staff",
                buttonString: "Try Again");
          }


        break;
      case 2:
       controller.forward();
          final actionStatus = await doCheckIn(busNumber, phoneNumber, vaLoginModel.data.id);
          controller.reverse();
          if (actionStatus == 200) {
            doResponseFeedback(
              context: context,
                title: "Check out successful",
                content: "Staff has been checked-out successfully",
                buttonString: "OK");
          }
          else {
            doResponseFeedback(
                context: context,
                title: "Try Again",
                content: "Something went wrong, while trying to check out staff",
                buttonString: "Try Again");
          }

        break;
    }
  }


  doResponseFeedback({BuildContext context, String title, content, buttonString }) async {
    print("inside life");
    await showDialog(
      context:  context,
      barrierDismissible: true,
      builder: (context) => AlertDialog(
        title:  Text(title, style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),),
        content: Text(content),
        actions: [
          new FlatButton(
            child: new Text(buttonString, style: TextStyle(color: Colors.yellow[800], fontWeight: FontWeight.bold),),
            onPressed: () {
              Navigator.pop(context);
              //Navigator.pop(context);

              if(buttonString == "OK") {
                _staffPhoneController.clear();
                Navigator.pop(context);
              }
              // Navigator.push(context, MaterialPageRoute(builder: (context) => PayOptionsPage()));
            }
            ,
          ),
        ],
      ),
    );
  }
}
