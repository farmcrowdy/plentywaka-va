import 'package:flutter/material.dart';
import 'dart:async';
import 'colors.dart';
import 'package:http/http.dart' as http;
import 'update_app/updateapp.dart';
import 'package:async_loader/async_loader.dart';
import 'dashboard/dashboardpage.dart';
import 'login/loginpage.dart';
import 'package:shared_preferences/shared_preferences.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PlentyWaka Corporate VA',
      theme: ThemeData(
        primarySwatch: Colors.yellow,
      ),
      home: MyHomePage(title: 'PlentyWaka Corporate VA'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  Future<int>  checkIfShouldReferUpdate() async {
    int action = 0;
    //await http.get("https://api.sheety.co/d4927a12-3ac8-42fa-9ecd-503eca2936d5").then((response) {
    //if(response.statusCode == 200) {
    //final vsControlPojo = vsControlPojoFromJson(response.body);
    //if(vsControlPojo[0].version > appCurrentVersion) {
    // _vsControlPojor = vsControlPojo[0];
    //action = 1;
    // return true;
    // }
    //else return false;
    // }
    //else return false;
    // }).catchError((onError) {
    //return false;
    // });

    SharedPreferences prefs = await SharedPreferences.getInstance();
    int counter = (prefs.getInt('loginStatus') ?? 0);
    return counter;
  }


  @override
  Widget build(BuildContext context) {


    Widget resultFromUpdates(int data) {
      switch(data) {
        case 1:
          return UpdateYourApp(null);
          break;
        default:
        // return DashboardPage();
          return LoginPage();
          //Navigator.pushReplacement(context, route);
          break;
      }
      //return SizedBox(width: 0, height: 0,);
    }


    var _asyncLoader = new AsyncLoader(
      initState: () async => await checkIfShouldReferUpdate(),
      renderLoad: () => Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/pw_logo_jpg.jpg', width: 160, height: 160,),
            SizedBox(height: 8,),
            Container(
                padding: EdgeInsets.all(26),
                child: Text('Bus Hailing Just Got Easier', style: TextStyle(fontSize: 26, color: primaryColor, fontWeight: FontWeight.bold), textAlign: TextAlign.center,))
          ],
        ),
      ),
      renderSuccess: ({data}) => resultFromUpdates(data),
    );



    return Scaffold(
        backgroundColor: Colors.white,
        body: _asyncLoader // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
